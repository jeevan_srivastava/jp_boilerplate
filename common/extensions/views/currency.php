<?php 
$activeCurrency = Currency::model()->findAllByAttributes(array('active' => '1'));
$allCurrency = array();
for($x=0;$x<count($activeCurrency);$x++){
	$allCurrency[$activeCurrency[$x]->id] = $activeCurrency[$x]->currency_name;
}
echo CHtml::form(); 
?>
    <div id="currencydrop">
        <?php echo CHtml::dropDownList('currency', Yii::app()->session['currency'], $allCurrency, array('submit' => '')); ?>
    </div>
<?php echo CHtml::endForm(); ?>
