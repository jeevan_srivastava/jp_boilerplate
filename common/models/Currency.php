<?php

/**
 * This is the model class for table "tbl_currency".
 *
 * The followings are the available columns in table 'tbl_currency':
 * @property integer $id
 * @property string $currency_name
 * @property string $currency_symbol
 * @property integer $initial_amount
 * @property integer $final_amount
 * @property integer $base_currency
 * @property integer $active
 *
 * The followings are the available model relations:
 * @property LoopCurrencyList[] $loopCurrencyLists
 */
class Currency extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Currency the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_currency';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('currency_name, currency_symbol, initial_amount, final_amount', 'required'),
			array('initial_amount, final_amount, base_currency, active', 'numerical', 'integerOnly'=>true),
			array('currency_name, currency_symbol', 'length', 'max'=>20),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, currency_name, currency_symbol, initial_amount, final_amount, base_currency, active', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'loopCurrencyLists' => array(self::HAS_MANY, 'LoopCurrencyList', 'currency_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'currency_name' => 'Currency Name',
			'currency_symbol' => 'Currency Symbol',
			'initial_amount' => 'Initial Amount',
			'final_amount' => 'Final Amount',
			'base_currency' => 'Base Currency',
			'active' => 'Active',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('currency_name',$this->currency_name,true);
		$criteria->compare('currency_symbol',$this->currency_symbol,true);
		$criteria->compare('initial_amount',$this->initial_amount);
		$criteria->compare('final_amount',$this->final_amount);
		$criteria->compare('base_currency',$this->base_currency);
		$criteria->compare('active',$this->active);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}