<?php

/**
 * SiteController.php
 *
 * @author: Hemc <hemendra@t9l.com>
 * 
 */
class SiteController extends Controller {

    /**
     * @return array list of action filters (See CController::filter)
     */
    public function filters() {
        return array('accessControl');
    }

    public function accessRules() {
        return array(
            // not logged in users should be able to login and view captcha images as well as errors
            array('allow', 'actions' => array('index', 'captcha', 'login', 'error', 'KK', 'check', 'register','verifynote','forgotPassword','lang')),
            // logged in users can do whatever they want to
            array('allow', 'users' => array('@')),
            // not logged in users can't do anything except above
            array('deny'),
        );
    }

    /**
     * Declares class-based actions.
     * @return array
     */
    public function actions() {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha' => array(
                'class' => 'CCaptchaAction',
                'backColor' => 0xFFFFFF,
            ),
            'page' => array(
                'class' => 'CViewAction',
            ),
        );
    }

    /* open on startup */

    public function actionIndex() {
        $this->render('index');
    }

    /**
     * This is the action to handle external exceptions.
     */
    public function actionError() {
        $error = Yii::app()->errorHandler->error;
        if (isset($error)) {
            if ($error['code'] == 404 || $error['code'] == 403) {
                $this->redirect($this->createUrl('/site/page',array('view'=>'error404')));
                die;
            }
            if($error['code']==500){
                  $this->redirect($this->createUrl('/site/page',array('view'=>'error500')));
                die;
            }
        }
        if (isset($_REQUEST['q'])) {
            $msg = $_REQUEST['q'];
            $decrypted = $this->decode($msg);
            /*
             * map here error array or from db to custom message
             */
            $this->render('error', array('message' => $decrypted));
        } else {
            $this->render('error');
        }
    }

    /**
     * Displays the login page
     */
    public function actionLogin() {

        $model = new LoginForm();

        if (isset($_POST['ajax']) && $_POST['ajax'] === 'login-form') {
            echo CActiveForm::validate($model, array('username', 'password', 'verifyCode'));
            Yii::app()->end();
        }
        
        if (isset($_POST['LoginForm'])) {
            $model->attributes = $_POST['LoginForm'];
            if ($model->validate(array('username', 'password', 'verifyCode')) && $model->login())
                $this->redirect(user()->returnUrl);
        }

        $sent = r()->getParam('sent', 0);
        $this->render('login', array(
            'model' => $model,
            'sent' => $sent,
        ));
    }

    /**
     * Logs out the current user and redirect to homepage.
     */
    public function actionLogout() {
        Yii::app()->user->logout();
        $this->redirect(Yii::app()->homeUrl);
    }

    public function actionCheck() {
       Yii::app()->user->setFlash('register_success','An email has been send to your username. Please verify it to login');
                    $this->redirect('/site/verifynote');
        
    }

    public function actionRegister() {
        $model=new User('checkout');
        if(!empty($_POST['User'])){
            //echo '<pre>';print_r($_POST['User']);die;
            $model->attributes = $_POST['User'];
            //echo '<pre>';print_r($model);
            //$model->validate();
            if($model->validate()){
                $model->password=$_POST['User']['newPassword'];
                $verification_code = rand(1, 1000000000000);
                $model->verification_code = base64_encode($verification_code);
                $model->status=0;
                if($model->save()){
                    $href = Yii::app()->params['protocol'] . $_SERVER['HTTP_HOST'] . '/user/verify?code=' . base64_encode($model->id . '&' . 'vc=' . $model->verification_code);
                    //echo $href;die;
                    $body='click on link to verify email<a href="#href#">click here</a>';
                    $body = str_replace("#href#", $href, $body);
                    $subject='Account verification';
                    $fromEmail=Yii::app()->params['adminEmail'];
                    $this->sentmail($model->username,$body,$subject,$fromEmail);
                    Yii::app()->user->setFlash('register_success','An email has been send to your username. Please verify it to login');
                    $this->redirect('/site/verifynote');
                }
            }
            //print_r($model->getErrors()); die;
        }
        $this->render('register',array('model'=>$model));

    }
    public function actionVerifynote(){
        $this->render('verifynote');
    }
    
      public function actionContact() {
        $model = new ContactForm;
        if (isset($_POST['ContactForm'])) {
            $model->attributes = $_POST['ContactForm'];
            if ($model->validate()) {
                /** example code */
//				$name='=?UTF-8?B?'.base64_encode($model->name).'?=';
//				$subject='=?UTF-8?B?'.base64_encode($model->subject).'?=';
//				$headers="From: $name <{$model->email}>\r\n".
//					"Reply-To: {$model->email}\r\n".
//					"MIME-Version: 1.0\r\n".
//					"Content-type: text/plain; charset=UTF-8";
//
//				mail(Yii::app()->params['adminEmail'],$subject,$model->body,$headers);
//				Yii::app()->user->setFlash('contact','Thank you for contacting us. We will respond to you as soon as possible.');
//				$this->refresh();
            }
        }
        $this->render('contact', array('model' => $model));
    }
    
   public function actionChange_forgotpassword($p = null) {
        if (isset($p)) {
            $isUserIdExist = UserForgotPassword::model()->findByAttributes(array('passcode' => $p));
            if (!$isUserIdExist) {
                $this->redirectError('INVALID_REQUEST');
            }
        } else {
            $isUserIdExist = UserForgotPassword::model()->findByAttributes(array('passcode' => $_POST['passcode']));
        }
// print_r($_POST); //exit;
// echo Yii::app()->user->email;
        if (isset(Yii::app()->user->email) && Yii::app()->user->email != User::model()->getemail(@$isUserIdExist->user_id)) {
// if (isset(Yii::app()->user->email) && !(isset($_POST['password']) && (@$_POST['confirm'] ))) {
//     if ($isUserIdExist && Yii::app()->user->email != @$isUserIdExist->user_id) {
//         $this->redirectError('ALREADY_LOGIN_WITH_OTHER_ACCOUNT');
//     }
// }
            $this->redirectError('ALREADY_LOGIN_WITH_OTHER_ACCOUNT');
        }

        if (isset($_POST['password']) && (@$_POST['confirm'] )) {
            $record_check_inforgot = UserForgotPassword::model()->findByAttributes(array('passcode' => $_POST['passcode']));
            if (isset($record_check_inforgot)) {

                $user_record = User::model()->findByAttributes(array('id' => $record_check_inforgot->user_id));
                if (!$user_record) {
                    $this->redirectError('NO_RECORD_FOUND');
                }
                if ($user_record->salt == null || $user_record->salt == '') {
                    @$salt = @$user_record->generateSalt();
                    @$user_record->salt = @$salt;
                }
                $salt = $user_record->salt;
//echo $salt; echo $_POST['password'];
                $pass = $_POST['password'];
                $passpattern = "/^(?=.*?[a-z])(?=.*?\d)/";
                $checkpassword = preg_match($passpattern, $pass);
                if (!$checkpassword || strlen($pass) < 6) {
//set flash here to let user know that password does't match to pattern.
                    Yii::app()->user->setFlash('pass_pattern_flash', 'The password should be atleast 6 characters and must contain one number');
                    $this->redirect('/user/change_forgotpassword');
                }
                $user_record->password = crypt($_POST['password'], $salt);
                $user_record->save();
                $record_check_inforgot->delete();
                Yii::app()->user->logout();
                $this->render('afterverification', array('message' => 'Your password has been reset successfully. Redirecting you to the homepage.'));
                die();
            } else {
                $this->redirectError('INVALID_REQUEST');
            }
        }
        $this->render('change_forgotpassword', array('p' => $p,));
    }
    
    public function actionLang(){
        $this->render('eventgift');
    }
    
}