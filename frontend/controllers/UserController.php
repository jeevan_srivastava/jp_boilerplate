<?php

class UserController extends Controller {

    // Uncomment the following methods and override them if needed
    /**
     * @return array list of action filters (See CController::filter)
     */
    public function filters() {
        return array('accessControl');
    }

    public function accessRules() {
        return array(
            // not logged in users should be able to login and view captcha images as well as errors
            array('allow', 'actions' => array('index', 'verify', 'reverify', 'recovery', 'ChangeForgotpassword')),
            // logged in users can do whatever they want to
            array('allow', 'users' => array('@')),
            // not logged in users can't do anything except above
            array('deny'),
        );
    }

    /*
      public function filters()
      {
      // return the filter configuration for this controller, e.g.:
      return array(
      'inlineFilterName',
      array(
      'class'=>'path.to.FilterClass',
      'propertyName'=>'propertyValue',
      ),
      );
      }

      public function actions()
      {
      // return external action classes, e.g.:
      return array(
      'action1'=>'path.to.ActionClass',
      'action2'=>array(
      'class'=>'path.to.AnotherActionClass',
      'propertyName'=>'propertyValue',
      ),
      );
      }
     */

    public function actionIndex() {
        $this->render('index');
    }

    ## Start :: function to Verify account after registration or through mail

    public function actionVerify($code = null) {
        $model = new User();
        //echo $code;die;
        if (isset($code)) {  # code come from mail
            $data = explode('&', base64_decode($code));
            $id = $data[0];
            $data1 = explode('vc=', base64_decode($code));
            $vc = @$data1[1];
            if (isset($id) && isset($vc)) {
                $user = User::model()->findByAttributes(array('id' => @$id));
            }
        }
        //echo $user->verification_code;echo '<br/>';
        //echo $vc;
        if (isset($user->status)) {
            if ($user->status != 1) {
                if ($user->verification_code == $vc) {
                    //echo "verification is  done";  die;                  
                    $user->status = 1;
                    $user->verification_code = 'NULL';
                    $user->update();
                    Yii::app()->user->setFlash('mail_verified', 'Your email has been verified. Please login');
                    $this->redirect('/site/login');
                } else {
                    $this->redirectError("Invalid_LINK");
                }
            } else {
                Yii::app()->user->setFlash('mail_verified', 'Your email has been verified already. Please login here');
                $this->redirect('/site/login');
            }
        } else {
            $this->redirectError("Undefined_user_status");
        }
    }

    public function actionreverify($username = null) {
        if (isset($username)) {
            $user = User::model()->findByAttributes(array('username' => @$username));
            if (!$user) {
                $this->redirectError('NO_RECORD_FOUND');
            }
            $verification_code = rand(1, 1000000000000);
            if (@$user->verification_code == '' || @$user->verification_code == null) {//print_r($user); exit;
                $user->verification_code = base64_encode($verification_code);
                $user->save();
            }
            $href = Yii::app()->params['protocol'] . $_SERVER['HTTP_HOST'] . '/user/verify?code=' . base64_encode($user->id . '&' . 'vc=' . $user->verification_code);
            //echo $href;die;
            $body = 'click on link to verify email<a href="#href#">click here</a>';
            $body = str_replace("#href#", $href, $body);
            $subject = 'Account verification';
            $fromEmail = Yii::app()->params['adminEmail'];
            $this->sentmail($user->username, $body, $subject, $fromEmail);
            Yii::app()->user->setFlash('register_success', 'An email has been send to your username. Please verify it to login');
            $this->redirect('/site/verifynote');
        } else {
            $this->redirectError('INVALID_REQUEST');
        }
    }

    /**
     * Recovery password
     */
    public function actionRecovery() {
        $form = new UserRecoveryForm;
        if (Yii::app()->user->id) {
            //echo Yii::app()->user->returnUrl;die;
            $this->redirect(Yii::app()->user->returnUrl);
        } else {

            if (isset($_POST['UserRecoveryForm'])) {
                //printdie($_POST);
                $form->attributes = $_POST['UserRecoveryForm'];
                if ($form->validate()) {
                    $user = User::model()->notsafe()->findbyPk($form->user_id);

                    # to check id already exist in forgot table
                    $forgotModel = UserForgotPassword::model()->findByAttributes(array('user_id' => $user->id));
                    if (isset($forgotModel->id)) {
                        $forgotModel->created = date('Y-m-d');
                        $forgotModel->save();
                    } else {
                        $forgotModel = new UserForgotPassword();
                        $forgotModel->user_id = $user->id;
                        //echo $forgotModel->user_id;
                        $forgotModel->passcode = base64_encode(rand(999999999, 100000000000) . $user->id . rand(999999999, 100000000000));
                        $forgotModel->created = date('Y-m-d');
                        $forgotModel->save();
                    }

                    $activation_url = Yii::app()->params['protocol'] . $_SERVER['HTTP_HOST'] . $this->createUrl('/user/changeforgotpassword', array('p' => $forgotModel->passcode));

                    $subject = Yii::t('recovery', "You have requested the password recovery site {site_name}", array(
                                '{site_name}' => Yii::app()->name,
                            ));
                    $message = Yii::t('recovery', "You have requested the password recovery site {site_name}. To receive a new password, go to {activation_url}.", array(
                                '{site_name}' => Yii::app()->name,
                                '{activation_url}' => $activation_url,
                            ));
                    $this->sentmail($user->username, $message, $subject, Yii::app()->params['adminEmail']);

                    Yii::app()->user->setFlash('recoveryMessage', Yii::t('recovery', "Please check your email. An instructions was sent to your email address."));
                    $this->refresh();
                }
            }
            $this->render('recovery', array('form' => $form));
        }
    }

    public function actionChangeForgotpassword($p = null) {
//                    if(!empty($_REQUEST))
//                    printdie($_REQUEST);

        if (isset($p)) {
            $userForgotModel = UserForgotPassword::model()->findByAttributes(array('passcode' => $p));
        } elseif(!empty ($_POST['passcode'])) {
            // in case of change password form submit
            $p=$_POST['passcode'];
            $userForgotModel = UserForgotPassword::model()->findByAttributes(array('passcode' => $p));
        }
        if ($userForgotModel) {

                    $form2 = new UserChangePassword;
                    if (isset($_POST['UserChangePassword'])) {
                        $user_record = User::model()->findByAttributes(array('id' => $userForgotModel->user_id));
                        if (!$user_record) {
                            $this->redirectError('NO_RECORD_FOUND');
                        }
                        $form2->attributes = $_POST['UserChangePassword'];
                        if ($form2->validate()) {
                                    $user_record->password =$form2->password;
                                    $user_record->status =1;
                                    if($user_record->update()){
                                    Yii::app()->user->setFlash('mail_verified', Yii::t('recovery', "New password is saved. Please login here"));
                                    Yii::app()->user->returnUrl=array('/site/index');
                                    $this->redirect('/site/login');
                                    }  else {
                                    Yii::app()->user->setFlash('change_password', Yii::t('recovery', "Unable to save data. Please try again"));
                                        
                                    }
                        }
                        
                    }
                    $this->render('changepassword', array('p' => $p,'form'=>$form2));
        } else {
            Yii::app()->user->setFlash('recoveryFailMessage', Yii::t('recovery', "invalid url Or Passcode.Please send activation url again"));
            $this->redirect('/user/recovery');
        }







        $email = ((isset($_GET['email'])) ? $_GET['email'] : '');
        $activkey = ((isset($_GET['activkey'])) ? $_GET['activkey'] : '');
        if ($email && $activkey) {
            $form2 = new UserChangePassword;
            $find = User::model()->notsafe()->findByAttributes(array('email' => $email));
            if (isset($find) && $find->activkey == $activkey) {
                if (isset($_POST['UserChangePassword'])) {
                    $form2->attributes = $_POST['UserChangePassword'];
                    if ($form2->validate()) {
                        $find->password = Yii::app()->controller->module->encrypting($form2->password);
                        $find->activkey = Yii::app()->controller->module->encrypting(microtime() . $form2->password);
                        if ($find->status == 0) {
                            $find->status = 1;
                        }
                        $find->save();
                        Yii::app()->user->setFlash('recoveryMessage', Yii::t('recovery', "New password is saved."));
                        $this->redirect(Yii::app()->controller->module->recoveryUrl);
                    }
                }
                $this->render('changepassword', array('form' => $form2));
            } else {
                Yii::app()->user->setFlash('recoveryMessage', Yii::t('recovery', "Incorrect recovery link."));
                $this->redirect(Yii::app()->controller->module->recoveryUrl);
            }
        }
    }

}