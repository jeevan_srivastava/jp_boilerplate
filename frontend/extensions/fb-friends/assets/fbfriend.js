/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


function strstr(haystack, needle, bool) {
    var pos = 0;

    haystack += "";
    haystack=haystack.toLowerCase();
    needle=needle.toLowerCase();

    pos = haystack.indexOf(needle);
    if (pos == -1) {
        return false;
    } else {
        if (bool) {
            return haystack.substr(0, pos);
        } else {
            return haystack.slice(pos);
        }
    }
}
 function callcancellight()
     {   
         $('#heading').html('Cancel Event');
         $('#popupcontent').html('<p> Do You really want to cancel event ?</p><a href="/event/remove" onclick="" class="btn close">Ok</a><a href="javascript:void(0);" onclick="" class="btn close">cancel</a>');      
         $('#modal').reveal({ // The item which will be opened with reveal
             animation: 'fade',                   // fade, fadeAndPop, none
             animationspeed: 600,                       // how fast animtions are
             closeonbackgroundclick: true,              // if you click background will modal close?
             dismissmodalclass: 'close'    // the class of a button or element that will close an open modal
         }); return true;
     }
function callCancel(){
    if(confirm("Do you really want to cancel event?")){
        window.location = "/event/remove";
    }
}

function eventCreate(){
    document.getElementById('createNewEvent').submit();
}

function processDetails(giftToId, giftToName,bday) 
{
    $("#to").attr("value", giftToId);
    $("#toname").attr("value", giftToName);
    $("#bday").attr("value", bday);

    document.forms["sendgift"].submit();
    return false;
}

function processFestivals(festivalid,fday) 
{
    $("#festivalid").attr("value", festivalid);
    $("#fday").attr("value", fday);

    document.forms["festivalgift"].submit();
    return false;
}

function searchFriend(ul_id,id_cnt)
{
    if(document.getElementById("lyr1"))document.getElementById("lyr1").style.top=0;
    if(document.getElementById("wn1_dragBar"))document.getElementById("wn1_dragBar").style.top=1;

    if(document.getElementById("lyr2"))document.getElementById("lyr2").style.top=0;
    if(document.getElementById("wn2_dragBar"))document.getElementById("wn2_dragBar").style.top=1;

    if(document.getElementById("lyr3"))document.getElementById("lyr3").style.top=0;
    if(document.getElementById("wn3_dragBar"))document.getElementById("wn3_dragBar").style.top=1;
    
    if(document.getElementById("lyr4"))document.getElementById("lyr4").style.top=0;
    if(document.getElementById("wn4_dragBar"))document.getElementById("wn4_dragBar").style.top=1;
    
    if(document.getElementById("lyr5"))document.getElementById("lyr5").style.top=0;
    if(document.getElementById("wn5_dragBar"))document.getElementById("wn5_dragBar").style.top=1;
    
    if(document.getElementById("lyr6"))document.getElementById("lyr6").style.top=0;
    if(document.getElementById("wn6_dragBar"))document.getElementById("wn6_dragBar").style.top=1;
  
    var key=document.getElementById("txtsearchfriend"+id_cnt+"").value;
    //alert(key);
    $.each($(ul_id+" li"), function(i, obj){
        obj = $(obj);
    
        if(strstr(obj.attr("fbname"),key)||key==""||key=="type friends name"||key=="start typing a name")obj.show();
        else obj.hide();
    
    });
    return false;
}

function showfriend(ul_id,id_cnt)
{
    $.each($(ul_id+" li"), function(i, obj){
        obj = $(obj);
        obj.show();
    });

}

function toggleID(fid)
{
    $("#"+fid).toggle();
    return false;
}

function setVar(fid,val)
{
    document.getElementById(fid).value=val;
    return false;
}
function setHtml(fid,val)
{
    document.getElementById(fid).innerHTML=val;
    return false;
}

var thismultiselect='';
function toggleMultiInvite(var_id,cnt_id)
{
    if(thismultiselect!='')document.getElementById(thismultiselect).innerHTML='';
    thismultiselect="inviteLb"+cnt_id;
  
    eval("document.getElementById('"+var_id+"').innerHTML="+var_id+";");
    $("#"+var_id).show();
  
    $.fcbkListSelection("#fcbklist"+cnt_id,"820","50","4",cnt_id);
    $("#fbselect"+cnt_id).click(function()
    {
        $('#fcbklist'+cnt_id).children().each(function(i,listEl){
            if(!$(listEl).hasClass("liselected")) {
                // item is not selected, so remove the form field value
                $(listEl).find("input").attr('checked', false);
            } else {
                $(listEl).find("input").attr('checked', true);
            }
        });
        $("#selectfacebookfriends"+cnt_id).submit();
    });
}
function hideMultiInvite(var_id,cnt_id)
{
    document.getElementById(var_id).innerHTML='';
    thismultiselect="";
}

$(window).load(function() {
    $('.delayedimg').each(function(){
        $(this).attr('src', $(this).attr('oldsrc'));
    });
});

function daysBetween(t1, t2) {
    //Total time for one day
    var one_day=1000*60*60*24; 
    //Here we need to split the inputed dates to convert them into standard format for furter execution
    var x=t1.split("/");     
    var y=t2.split("/");
    //date format(Fullyear,month,date) 
    var date1=new Date(x[2],(x[1]-1),x[0]);
    var date2=new Date(y[2],(y[1]-1),y[0])
    var month1=x[1]-1;
    var month2=y[1]-1;
    //Calculate difference between the two dates, and convert to days               
    _Diff=Math.ceil((date2.getTime()-date1.getTime())/(one_day)); 
    return _Diff;   
} 

 function cancelrequest(data)
     {
        
         var token=$('input[name="YII_CSRF_TOKEN"]').val();
         $.ajax({
             url:'/user/facebook_associate_cancelrequest',
             data:{request:data,YII_CSRF_TOKEN:token},
             type:"POST",
             dataType:"html",
             async:false,
             success:function(response){
                
                 if(response==0){
                     // $('#cancelpassword_success').html('Request is Cancelled Successfully');
                     $('#cancelpassword_error').html("");
                     $('#removepassword_error').html("");
                     $('#removepassword_success').html('');
                     location.reload();
                                 

                 }
                 else if(response==1)
                             
                 {  
                     // $('#cancelpassword_success').html('Request is Cancelled Successfully');
                     $('#cancelpassword_error').html("");$('#cancelpassword_error').html("");
                     $('#removepassword_error').html("");
                     $('#removepassword_success').html('');location.reload();}
                             
                 else{ 
                     // $('#cancelpassword_error').html('Request is not Cancelled. Try Again..');
                     $('#cancelpassword_error').html("");
                     $('#removepassword_error').html("");
                     $('#removepassword_success').html('');
                     location.reload(); 
                     return false;
                 }              
             } 
         });
       
     }
     
     
     function check(){
         alert('oh i am from assets');
     }
