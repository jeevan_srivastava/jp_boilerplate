<?php

/**
 * Yii extention widget 
 * @author Hemc <hemendra.chaudhary619@gmail.com>
 * @function for showing all facebook friends.
 */
class FbFriendWidget extends CWidget {

    public $width = "full";
    private $assetsUrl;
    
    public function run() {
       
        $this->assetsUrl=Yii::app()->assetManager->publish(
                        Yii::getPathOfAlias('application.extensions') . '/fb-friends/assets'
               );
        
        
        
        $facebook = Yii::app()->facebook;
        $facebook_uid = '';
        try {
            $facebook_uid = $facebook->getUser();
        } catch (FacebookApiException $e) {
            $facebook->destroySession();
            exit;
        }

        if ($facebook_uid != '') {
            try {
                $accessToken = $facebook->getAccessToken();
            } catch (Exception $e) {
                exit;
            }
        }
        // $fbuser = $facebook->api('/me?access_token=' . $accessToken);

        if (isset($facebook_uid) && $facebook_uid != 0) {
            //echo "here"; echo $facebook_uid;die;
            //echo $accessToken;die;
            $fromDate = date('m/d');
            $past_birthdays = "";
            try {
                if (isset($accessToken)) {
                    $upcoming_birthdays = $facebook->api(array(
                        'method' => 'fql.query',
                        'query' => "SELECT uid, name, birthday, birthday_date, substr(birthday_date, 0,3) from user where uid in (select uid2 from friend where uid1=me()) and birthday_date >='$fromDate' order by birthday_date Limit 0,500",
                        'access_token' => $accessToken
                            ));
                    $no = count($upcoming_birthdays); //exit;
                    if ($no < 500) {
                        $Limit2 = 500 - $no;
                        $past_birthdays = $facebook->api(array(
                            'method' => 'fql.query',
                            'query' => "SELECT uid, name, birthday, birthday_date, substr(birthday_date, 0,3) from user where uid in (select uid2 from friend where uid1=me()) and birthday_date >='01/01' and birthday_date <= '$fromDate' order by birthday_date Limit 0,$Limit2",
                            'access_token' => $accessToken
                                ));
                        //echo count($past_birthdays);exit;
                        $upcoming_birthdays = array_merge($upcoming_birthdays, $past_birthdays);
                        //echo count($upcoming_birthdays);exit;
                        
                    }
                }
            } catch (Exception $e) {
                $upcoming_birthdays = 0;
            }
            if ($this->width == 'small') {
                $this->render('fbUpcomingBday', array('assetsUrl'=>$this->assetsUrl,'upcoming_birthdays' => $upcoming_birthdays));
            } else {
                $this->render('fbUpcomingbdayFull', array('upcoming_birthdays' => $upcoming_birthdays));
            }
        }
    }

}

?>
