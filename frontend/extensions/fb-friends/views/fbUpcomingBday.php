
<?php


$start_ts = time();

function idx(array $array, $key, $default = null) {
    return array_key_exists($key, $array) ? $array[$key] : $default;
}

function he($str) {
    return htmlentities($str, ENT_QUOTES);
}
?>
    <?php
    $i = 0;
    if ($upcoming_birthdays) {
        foreach ($upcoming_birthdays as $ub):

            $id = he(idx($ub, 'uid'));
            $name = idx($ub, 'name');
            //$name=json_decode("\"$name\"");
            //$birthday = idx($ub, 'birthday_date');
            $birthday = substr(idx($ub, 'birthday_date'), 0, 5);

            $giftyear = date('Y');
            $bday = explode("/", idx($ub, 'birthday_date'));
            if ($bday[0] < date('m'))
                $giftyear+=1;

            $end_ts = strtotime($giftyear . '-' . $bday[0] . "-" . $bday[1]);
            $diff = $end_ts - $start_ts;
            $daysleft = floor($diff / 86400) + 1;
            // sometimes facebook api return negative time to the b'days those lies in 12th month .
            // (means past month b'day for next years) that leads to show 363 days as -2 days.
            if ($daysleft < 0) {
                $daysleft = 365 + $daysleft;
            }
            ?>

            <li fbname='<?php echo $name ?>'>
                <div class=days> <?php if ($daysleft == 0) {
                echo "<small>Today</small> ";
            } else {
                echo '<span>' . $daysleft . '</span> <small>days </small>';
            } ?> </div>
                <div class=pic> <a style="display:inline;" href=# onclick="processDetails('<?php echo $id . "','" . $name . "','" . $birthday ?>');return false;"> 
                        <img src='https://graph.facebook.com/<?php echo $id ?>/picture?type=square' width=50 height=50 /> </a> 
                </div>
                <div class=name>
                    <h3><a href='#' onclick="processDetails('<?php echo $id . "','" . $name . "','" . $birthday ?>');return false;" target=_blank><?php echo $name ?></a> </h3>
                </div>
                <div class=send_gift> <a href=# onclick="processDetails('<?php echo $id . "','" . $name . "','" . $birthday ?>');return false;"> <img src='../images/roof.png'/> <span> Send A Gift </span> </a> </div>
            </li>
        <?php endforeach; ?>
    <?php } ?>