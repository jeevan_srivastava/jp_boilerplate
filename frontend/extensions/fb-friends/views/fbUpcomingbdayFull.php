<?php
$start_ts = time();

function idx(array $array, $key, $default = null) {
    return array_key_exists($key, $array) ? $array[$key] : $default;
}

function he($str) {
    return htmlentities($str, ENT_QUOTES);
}
?>


<link rel="stylesheet" href="<?php echo Yii::app()->homeUrl ?>css/960_16_col.css" type="text/css" />
<link rel="stylesheet" href="<?php echo Yii::app()->homeUrl ?>css/css3.css" type="text/css" />


<form name="sendgift" id="sendgift" action="<?php echo Yii::app()->homeUrl . "event/sendbits"; ?>" method="post" >
    <input type="hidden" id="amount" name="amount" value=''>
    <input type="hidden" id="from" name="from" value="<?php echo Yii::app()->user->getId(); ?>" />
    <input type="hidden" id="to" name="to" value="" />
    <input type="hidden" id="bday" name="bday" value="" />
    <input type="hidden" id="toname" name="toname" value="" />
    <input type="hidden" value="<?php echo Yii::app()->getRequest()->getCsrfToken(); ?>" name="YII_CSRF_TOKEN" />
</form>

<form name="festivalgift" id="festivalgift" action="<?php echo Yii::app()->homeUrl . "event/festivalgift"; ?>" method="post" >
    <input type="hidden" id="from" name="from" value="<?php echo Yii::app()->user->getId(); ?>" />
    <input type="hidden" id="fday" name="fday" value="" />
    <input type="hidden" id="festivalid" name="festivalid" value="" />
    <input type="hidden" value="<?php echo Yii::app()->getRequest()->getCsrfToken(); ?>" name="YII_CSRF_TOKEN" />
</form>

<!--body_container start here-->
<div id="outer_body_container">
    <form action="#">
        <div class="main_container">
            <div class="step1 grid_16 beta">
                <div class="top_head">
                    <div class="left prefix_1 grid_7 alpha">
                        <h2>Upcoming <span>Birthdays</span></h2>
                    </div>
                    <div class="right grid_7">

                        <input type="text" value="type friends name" onfocus="if(this.value=='type friends name')this.value=''" onblur="if(this.value=='')this.value='type friends name'" id=txtsearchfriend onkeyup="return searchFriend('#friendlistUL','');" />

                    </div>
                </div>
                <div id='wn5' style="width:920px;height:500px;float:left;"> 

                    <div  id='lyr5' > 

                        <ul class="listing fes-listing" id=friendlistUL>

                            <?php
                            $i = 0;
                            if ($upcoming_birthdays) {
                                foreach ($upcoming_birthdays as $ub):
                                    $id = he(idx($ub, 'uid'));
                                    $name = idx($ub, 'name');
                                    //$birthday = idx($ub, 'birthday_date');
                                    $birthday = substr(idx($ub, 'birthday_date'), 0, 5);

                                    $giftyear = date('Y');
                                    $bday = explode("/", idx($ub, 'birthday_date'));
                                    if ($bday[0] < date('m'))
                                        $giftyear+=1;

                                    $end_ts = strtotime($giftyear . '-' . $bday[0] . "-" . $bday[1]);
                                    $diff = $end_ts - $start_ts;
                                    $daysleft = floor($diff / 86400) + 1;
                                    if ($daysleft < 0) {
                                        $daysleft = 365 + $daysleft;
                                    }
                                    ?>

                                    <li fbname='<?php echo $name ?>'>
                                        <div class="days grid_2 pad_40 alpha"> <?php
                            if ($daysleft == 0) {
                                echo "<small>Today</small> ";
                            } else {
                                echo '<span>' . $daysleft . '</span> <small>days </small>';
                            }
                            ?> </div>
                                        <div class="pic grid_2"> <a style="display:inline;" href=# onclick="processDetails('<?php echo $id . "','" . $name . "','" . $birthday ?>');return false;"> 
                                                <img src='https://graph.facebook.com/<?php echo $id ?>/picture?type=square' width=50 height=50 /> </a> 
                                        </div>
                                        <div class="name prefix_1 grid_7">
                                            <h3><a href='#' onclick="processDetails('<?php echo $id . "','" . $name . "','" . $birthday ?>');return false;" target=_blank><?php echo $name ?></a> </h3>
                                        </div>
                                        <div class="send_gift grid_2"> <a href=# onclick="processDetails('<?php echo $id . "','" . $name . "','" . $birthday ?>');return false;"> <img src='../images/roof.png'/> <span> Send A Gift </span> </a> </div>
                                    </li>

                                    <?php
                                endforeach;
                            }
                            ?>



                        </ul>
                        <!--                            <div class="send_gift grid_2" id="next_button_full"> 
                                                                     <a href="#" onclick="loadnextlist(<?php //echo $nextyear; echo ","; echo $uptobday; ?>);"> Next </a>
                                                    </div>-->
                    </div>
                </div> 
                <div id="scrollbar5" style='float:left;'></div>
            </div>
            <!--left section end here--> 

        </div>
        <input type="hidden" value="<?php echo Yii::app()->getRequest()->getCsrfToken(); ?>" name="YII_CSRF_TOKEN" />
    </form>
    <!--mid section end here--> 
</div>
<script type="text/javascript">
    function init_dw_Scroll() {
        // arguments: id of scroll area div, id of content div
            
        var wndo5 = new dw_scrollObj('wn5', 'lyr5');
        wndo5.buildScrollControls('scrollbar5', 'v', 'mouseover', true);
            
    }
    // if code supported, link in the style sheet (optional) and call the init function onload
    if ( dw_scrollObj.isSupported() ) {
        dw_Event.add( window, 'load', init_dw_Scroll);
    }
    function  loadnextlist(nextyear,uptobday){
        //console.log("load next function start");
        var token=$('input[name="YII_CSRF_TOKEN"]').val();
        $.ajax({
            type: "POST",
            url: "/user/upcomingBdayFull",
            data:{YII_CSRF_TOKEN:token,list:1,nextyear:nextyear,uptobday:uptobday}
        }).done(function(msg) {
            //$("#friendlistUL").append(msg);
            //console.log("response added to list");
            //var wndo5 = new dw_scrollObj('wn5', 'lyr5');
            //wndo5.buildScrollControls('scrollbar5', 'v', 'mouseover', true);
            //console.log("scrollbar added to appended list");
        });

    }

</script>

