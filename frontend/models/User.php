<?php

/**
 * User.php
 *
 * @author: Hemendra <hemendra@t9l.com>
 * 
 */

/**
 * This is the model class for table "tbl_user".
 *
 * The followings are the available columns in table 'tbl_user':
 * @property integer $id
 * @property string $fname
 * @property string $lname
 * @property string $dob
 * @property string $username
 * @property string $password
 * @property string $password_strategy
 * @property integer $requires_new_password
 * @property string $salt
 * @property integer $status
 * @property string $social_id
 * @property string $social_id_type
 * @property string $user_sex
 * @property string $user_photo
 * @property string $user_referrer_type
 * @property string $user_creation_ip
 * @property string $user_lastlogin_ip
 * @property string $verification_code
 * @property integer $has_agreed_terms
 * @property integer $is_age_eighteen_above
 * @property integer $logout_from_fb
 * @property integer $ask_to_logout
 * @property integer $is_locked_out
 * @property string $lock_out_time
 * @property integer $failed_atttempt_count
 * @property string $created
 * @property string $modified
 * @property string $last_login_time
 * @property integer $is_deleted
 *
 * The followings are the available model relations:
 * @property CardPaymentLog[] $cardPaymentLogs
 * @property Event[] $events
 * @property Event[] $events1
 * @property EventGallery[] $eventGalleries
 * @property EventGiftTransaction[] $eventGiftTransactions
 * @property EventInvite[] $eventInvites
 * @property EventInvite[] $eventInvites1
 * @property PaymentTransaction[] $paymentTransactions
 * @property UserAddress[] $userAddresses
 * @property UserAddressbook[] $userAddressbooks
 * @property UserCard[] $userCards
 * @property UserEmail[] $userEmails
 * @property UserFacebook[] $userFacebooks
 * @property UserForgotPassword[] $userForgotPasswords
 * @property UserFriend[] $userFriends
 */
class User extends CActiveRecord {

    /**
     * @var string attribute used for new passwords on user's edition
     */
    public $newPassword;

    /**
     * @var string attribute used to confirmation fields
     */
    public $passwordConfirm;

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return User the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'tbl_user';
    }

    /**
     * Behaviors
     * @return array
     */
    public function behaviors() {
        Yii::import('common.extensions.behaviors.password.*');
        return array(
            // Password behavior strategy
            "APasswordBehavior" => array(
                "class" => "APasswordBehavior",
                "defaultStrategyName" => "bcrypt",
                "strategies" => array(
                    "bcrypt" => array(
                        "class" => "ABcryptPasswordStrategy",
                        "workFactor" => 14,
                        "minLength" => 8
                    ),
                    "legacy" => array(
                        "class" => "ALegacyMd5PasswordStrategy",
                        'minLength' => 8
                    )
                ),
            )
        );
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
      
        return array(
            //on register time Scnerio on checkout
            array('username', 'required', 'on' => 'checkout','message' => Yii::t('validation', 'Email can not be blank.')),
            array('username', 'unique', 'on' => 'checkout', 'message' => Yii::t('validation', 'Email has already been taken.')),
            array('passwordConfirm', 'required', 'on' => 'checkout','message' => Yii::t('validation', 'Confirm password can not be blank')),
            array('passwordConfirm', 'compare', 'compareAttribute' => 'newPassword','on' => 'checkout', 'message' => Yii::t('validation', "Passwords don't match")),
            
            // general rules for user table
            array('username', 'unique'),
            array('username', 'email', 'message' => Yii::t('validation', 'Invalid username/email format ')),
            array('newPassword, password_strategy ', 'length', 'max' => 50, 'min' => 8),
            
            array('requires_new_password, status, has_agreed_terms, is_age_eighteen_above, logout_from_fb, ask_to_logout, is_locked_out, login_attempts, is_deleted', 'numerical', 'integerOnly' => true),
            array('fname, lname, username, verification_code', 'length', 'max' => 50),
            array('password', 'length', 'max' => 32),
           
            array('user_sex', 'length', 'max' => 8),
            array('user_photo', 'length', 'max' => 60),
           
            array('created, modified, last_login_time', 'safe'),
     
            array('id, fname, lname, dob, username, password, password_strategy, requires_new_password, salt, status, social_id, social_id_type, user_sex, user_photo, user_referrer_type, user_creation_ip, user_lastlogin_ip, verification_code, has_agreed_terms, is_age_eighteen_above, logout_from_fb, ask_to_logout, is_locked_out, lock_out_time, login_attempts, created, modified, last_login_time, is_deleted', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'cardPaymentLogs' => array(self::HAS_MANY, 'CardPaymentLog', 'user_id'),
            'events' => array(self::HAS_MANY, 'Event', 'event_receipient_id'),
            'events1' => array(self::HAS_MANY, 'Event', 'event_creator_id'),
            'eventGalleries' => array(self::HAS_MANY, 'EventGallery', 'user_id'),
            'eventGiftTransactions' => array(self::HAS_MANY, 'EventGiftTransaction', 'contributor_id'),
            'eventInvites' => array(self::HAS_MANY, 'EventInvite', 'inviter_id'),
            'eventInvites1' => array(self::HAS_MANY, 'EventInvite', 'invited_id'),
            'paymentTransactions' => array(self::HAS_MANY, 'PaymentTransaction', 'user_id'),
            'userAddresses' => array(self::HAS_MANY, 'UserAddress', 'user_id'),
            'userAddressbooks' => array(self::HAS_MANY, 'UserAddressbook', 'user_id'),
            'userCards' => array(self::HAS_MANY, 'UserCard', 'user_id'),
            'userEmails' => array(self::HAS_MANY, 'UserEmail', 'user_id'),
            'userFacebooks' => array(self::HAS_MANY, 'UserFacebook', 'user_id'),
            'userForgotPasswords' => array(self::HAS_MANY, 'UserForgotPassword', 'user_id'),
            'userFriends' => array(self::HAS_MANY, 'UserFriend', 'user_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'fname' => 'Fname',
            'lname' => 'Lname',
            'dob' => 'Dob',
            'username' => 'Username',
            'password' => 'Password',
            'password_strategy' => 'Password Strategy',
            'requires_new_password' => 'Requires New Password',
            'salt' => 'Salt',
            'status' => 'Status',
            'social_id' => 'Social',
            'social_id_type' => 'Social Id Type',
            'user_sex' => 'User Sex',
            'user_photo' => 'User Photo',
            'user_referrer_type' => 'User Referrer Type',
            'user_creation_ip' => 'User Creation Ip',
            'user_lastlogin_ip' => 'User Lastlogin Ip',
            'verification_code' => 'Verification Code',
            'has_agreed_terms' => 'Has Agreed Terms',
            'is_age_eighteen_above' => 'Is Age Eighteen Above',
            'logout_from_fb' => 'Logout From Fb',
            'ask_to_logout' => 'Ask To Logout',
            'is_locked_out' => 'Is Locked Out',
            'lock_out_time' => 'Lock Out Time',
            'login_attempts' => 'Failed Atttempt Count',
            'created' => 'Created',
            'modified' => 'Modified',
            'last_login_time' => 'Last Login Time',
            'is_deleted' => 'Is Deleted',
        );
    }

    /**
     * 
     */
    public function scopes()
    {
        return array(
            'notsafe'=>array(
            	'select' => 'id, username, password,status',
            ),
        );
    }
    
    /**
     * Helper property function
     * @return string the full name of the customer
     */
    public function getFullName() {

        return $this->username;
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('fname', $this->fname, true);
        $criteria->compare('lname', $this->lname, true);
        $criteria->compare('dob', $this->dob, true);
        $criteria->compare('username', $this->username, true);
        $criteria->compare('password', $this->password, true);
        $criteria->compare('password_strategy', $this->password_strategy, true);
        $criteria->compare('requires_new_password', $this->requires_new_password);
        $criteria->compare('salt', $this->salt, true);
        $criteria->compare('status', $this->status);
        $criteria->compare('social_id', $this->social_id, true);
        $criteria->compare('social_id_type', $this->social_id_type, true);
        $criteria->compare('user_sex', $this->user_sex, true);
        $criteria->compare('user_photo', $this->user_photo, true);
        $criteria->compare('user_referrer_type', $this->user_referrer_type, true);
        $criteria->compare('user_creation_ip', $this->user_creation_ip, true);
        $criteria->compare('user_lastlogin_ip', $this->user_lastlogin_ip, true);
        $criteria->compare('verification_code', $this->verification_code, true);
        $criteria->compare('has_agreed_terms', $this->has_agreed_terms);
        $criteria->compare('is_age_eighteen_above', $this->is_age_eighteen_above);
        $criteria->compare('logout_from_fb', $this->logout_from_fb);
        $criteria->compare('ask_to_logout', $this->ask_to_logout);
        $criteria->compare('is_locked_out', $this->is_locked_out);
        $criteria->compare('lock_out_time', $this->lock_out_time, true);
        $criteria->compare('login_attempts', $this->login_attempts);
        $criteria->compare('created', $this->created, true);
        $criteria->compare('modified', $this->modified, true);
        $criteria->compare('last_login_time', $this->last_login_time, true);
        $criteria->compare('is_deleted', $this->is_deleted);

        return new CActiveDataProvider($this, array(
                    'criteria' => $criteria,
                ));
    }

    /**
     * Makes sure usernames are lowercase
     * (emails by standard can have uppercase letters)
     * @return parent::beforeValidate
     */
    public function beforeValidate() {
        if (!empty($this->username))
            $this->username = strtolower($this->username);
        return parent::beforeValidate();
    }

    /**
     * Generates a new validation key (additional security for cookie)
     */
    public function regenerateValidationKey() {
        $this->saveAttributes(array(
            'validation_key' => md5(mt_rand() . mt_rand() . mt_rand()),
        ));
    }

}