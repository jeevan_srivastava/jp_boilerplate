<?php

/**
 * Controller.php
 *
 * @author: Hemendra <hemendra@t9l.com>
 * 
 */
class Controller extends CController {

    public $breadcrumbs = array();
    public $menu = array();

    /**
     * common error redirection for site
     * It will incryppt the message code 
     * so that it c\an be send in url 
     * @param String $ecode Message code or message itself
     */
    public function redirectError($ecode) {
        $value = $ecode;
        $querystring = $this->encode($value);
        $this->redirect(array('/site/error/', 'q' => $querystring));
    }

    /**
     * Common encryption function for platform
     * Config key in param file or config/main.php
     */
    function encode($value) {
        $skey = Yii::app()->params['skey']; // you can change it
        if (!$value) {
            return false;
        }
        $text = $value;
        $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB);
        $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
        $crypttext = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $skey, $text, MCRYPT_MODE_ECB, $iv);
        return trim($this->safe_b64encode($crypttext));
    }

    /**
     * Common decryption function for platform
     * Config key in param file or config/main.php
     */
    function decode($value) {
        $skey = Yii::app()->params['skey']; // you can change it
        if (!$value) {
            return false;
        }
        $crypttext = $this->safe_b64decode($value);
        $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB);
        $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
        $decrypttext = mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $skey, $crypttext, MCRYPT_MODE_ECB, $iv);
        return trim($decrypttext);
    }

    /**
     * Site version of base64_encode().
     * Security purpose
     */
    function safe_b64encode($string) {

        $data = base64_encode($string);
        $data = str_replace(array('+', '/', '='), array('-', '_', ''), $data);
        return $data;
    }

    /**
     * Site version of base64_decode().
     * Security purpose
     */
    function safe_b64decode($string) {
        $data = str_replace(array('-', '_'), array('+', '/'), $string);
        $mod4 = strlen($data) % 4;
        if ($mod4) {
            $data .= substr('====', $mod4);
        }
        return base64_decode($data);
    }
    
    /**
     * Send mail common function to all controllers
     * Using swiftmailer wrapper Yii extention 
     * see http://www.yiiframework.com/extension/mail/  
     */
    public function sentmail($toEmail,$body,$subject,$fromEmail){
            $message = new YiiMailMessage;
            $message->setBody($body);
            $message->subject = $subject;
            $message->addTo($toEmail);
            $message->from = $fromEmail;
            Yii::app()->mail->send($message);
    }


    protected function beforeRender($v = null) {
               if (isset($_POST['currency']) || isset(Yii::app()->session['currency'])) {
                   if (isset($_POST['currency'])) {
                       $activeCurrency = Currency::model()->countByAttributes(array('active' => '1', 'id' => $_POST['currency']));
                       if ($activeCurrency) {
                           Yii::app()->session['currency'] = $_POST['currency'];
                       }
                   }
               } else {
                   $activeCurrency = Currency::model()->findByAttributes(array('active' => '1', 'base_currency' => '1'));
                   Yii::app()->session['currency'] = $activeCurrency->id;
               }
       return true;
   }
}
