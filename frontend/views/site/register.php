<div class="container">                 
    <div class="">
        <h3 class="">Register<span> Your Account</span> </h3>                                    

        <p>Already registered? <a href="<?php echo $this->createurl('login') ?>" >Login here</a></p>

        <small class="">Fields marked with <span class="required">*</span> are mandatory.</small>
    </div>

    <div class="form">
        <div>
            <?php if (Yii::app()->user->hasFlash('notagree')): ?>
                <div style='' class=""> <?php echo Yii::app()->user->getFlash('notagree'); ?></div><?php endif; ?>
        </div>
        <div class=""> 

            <?php
            $form = $this->beginWidget('CActiveForm', array(
                'id' => 'register-form', 'action' => $this->createUrl('register'),
                'enableClientValidation' => true,
                'clientOptions' => array(
                    'validateOnSubmit' => true,
                    'afterValidate' => 'js:afterValidate',
                ),
                    ));
            ?>
            <?php //echo $form->errorSummary($model,"",array('class'=>'alert-error')); ?>
            <div class="">
                <label>First Name<span class="required">*</span></label>
<?php
if (!isset($fname) || (@$fname == '')) {
    $fname = 'Enter your first name';
}
?>
                <?php echo $form->textField($model, 'fname', array('size' => 40, 'value' => @$fname, 'maxlength' => 50, 'autocomplete' => 'off', 'id' => 'fname')); ?>
                <?php echo $form->error($model, 'fname', array('class' => 'alert-error')); ?>
                <div id='fname_error'  class="alert-error"></div>
            </div>

            <div class="">
                <label>Last Name<span class="required">*</span></label>
<?php
if (!isset($lname) || (@$lname == '')) {
    $lname = 'Enter your last name';
}
?>
                <?php echo $form->textField($model, 'lname', array('size' => 40, 'value' => @$lname, 'maxlength' => 50, 'autocomplete' => 'off', 'id' => 'lname')); ?>
                <?php echo $form->error($model, 'lname', array('class' => 'alert-error')); ?>
                <div id='lname_error' class="alert-error"></div>
            </div>

            <div class=""><label>Email Id<span class="required">*</span></label>
<?php
if (!isset($username) || (@$username == 'Enter email address')) {
    $username = 'Enter your email id';
}
?>
                <?php echo $form->textField($model, 'username', array('size' => 40, 'maxlength' => 50, 'value' => $username, 'autocomplete' => 'off')); ?>
                <?php echo $form->error($model, 'username', array('class' => 'alert-error')); ?>
                <div id='email_error' class="alert-error"></div>
            </div>

            <div class="">
                <label>Date of birth</label>
                <style>button.ui-datepicker-current { display: none; }</style>
<?php
$this->widget('zii.widgets.jui.CJuiDatePicker', array(
    'model' => $model,
    'attribute' => 'dob',
    'value' => $model->dob,
    'id' => 'dob',
    // 'readonly'=>'readonly',  
    // additional javascript options for the date picker plugin
    'options' => array(
        'showAnim' => 'fold',
        'dateFormat' => 'dd/mm/yy',
        'showButtonPanel' => true,
        //'defaultDate' => $model->IDueDate,
        //'defaultDate' => '1990-01-01',
        'changeYear' => true,
        'changeMonth' => true,
        'yearRange' => '1900',
    ),
    'htmlOptions' => array('size' => 36,
        'readonly' => 'readonly', 'class' => 'dob'
    ),
));
?><div id='dob_error' class="alert-error"></div>
                <?php echo $form->error($model, 'dob', array('class' => 'alert-error')); ?>
            </div>

            <div class="">
                <label>Choose a new password<span class="required">*</span></label>
<?php
if (!isset($password) || @$password == 'password') {
    $password = '';
}
?>
                <?php echo $form->passwordField($model, 'newPassword', array('size' => 40, 'maxlength' => 50, 'id' => 'password', 'value' => @$password)); ?>
                <?php echo $form->error($model, 'newPassword', array('class' => 'alert-error')); ?>
                <div id='password_error' class="alert-error"></div>
            </div>

            <div class="">
                <label>Confirm new password<span class="required">*</span></label>
<?php echo $form->passwordField($model, 'passwordConfirm', array('size' => 40, 'id' => 'confirm', 'width' => 100, 'maxlength' => 100, 'value' => '')); ?>
                <?php echo $form->error($model, 'passwordConfirm', array('class' => 'alert-error')); ?>
                <div id='confirm_error' class="alert-error"></div>
            </div>



            <div class="">
                <label class="">&nbsp;</label>
<?php echo CHtml::checkBox('User[agree]', '', array('style' => 'width:15px', 'id' => 'agree', 'value' => '1', 'uncheckValue' => '0')); ?>&nbsp;I confirm that I am above 18 and agree to the 
                <a href='/home/terms' target='_blank' >Terms and Conditions.</a>
                <div class="alert-error"  id="check_confirm"></div>
            </div>


            <div class="">
                <label class="">&nbsp;</label>
<?php echo CHtml::submitButton('register', array('class' => 'button', 'id' => 'register')); ?>
            </div>
                <?php $this->endWidget(); ?>
            <div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(function(){
        /* Hide form input values on focus*/ 
        $('input:text, input:password, textarea').each(function(){
            var txtval = $(this).val();
            $(this).focus(function(){
                if($(this).val() == txtval){
                    $(this).val('')
                }
            });
            $(this).blur(function(){
                if($(this).val() == ""){
                    $(this).val(txtval);
                }
            });
        });
    });
	
    $(document).ready(function (){
		
        $("#password").blur( function (){
            if (this.value.replace(/\s/g,'')=='') {
                this.value='Password';
                this.type='text';
            } 
        });
        $("#password").focus( function (){
            if (this.value=='Password'){
                this.value='';
                this.type='password';
            }
        });

    });
   
  
    function passwordval()
    { 
        if($('#confirm').val() !=$('#password').val())
        {
            $('#confirm_error').html('Password do not match')
            return false;
        }  else
        { 
            $('#confirm_error').html('');
        }
    }
	
    function passwordvalidation()
    { 
        return true;
        //        var passpateern=/^(?=.*?[a-z])(?=.*?\d)/;
        //        textlength=$('#password').val();
        //        if($('#password').val() =='')
        //        {
        //            $('#password_error').html('Password  cannot be blank');
        //            return false;
        //        }else if( textlength.length < 6 || !textlength.match(passpateern))
        //        {
        //            $('#password_error').html('The password should be atleast 6 characters and must contain one number');
        //            return false;
        //        }else
        //        { 
        //            $('#password_error').html('');
        //        }
    }

    
    function afterValidate(form, data, hasError)
    { 
         
        if (!hasError) 
        { 
            if($('#fname').val()=='' || $('#fname').val()=='Enter your first name')
            { $('#fname_error').html('First name cannot be blank');return false;}
            else
            { $('#fname_error').html('');}
            if($('#lname').val()=='' || $('#lname').val()=='Enter your last name')
            { $('#lname_error').html('Last name cannot be blank');return false;}
            else
            { $('#lname_error').html('');}
            textlength=$('#password').val();
            var passpateern=/^(?=.*?[a-z])(?=.*?\d)/;
            if($('#password').val() =='')
            {
                $('#password_error').html('Password  cannot  be blank');
                return false;
            } else  
            {
                $('#password_error').html('');
            }
               
            //            if( textlength.length < 6 || !textlength.match(passpateern))
            //            {
            //                $('#password_error').html('The password should be atleast 6 characters and must contain one number'); 
            //                return false;
            //            }else
            //            {
            //                $('#password_error').html('');
            //            }
            if($('#confirm').val() !=$('#password').val())
            {
                $('#confirm_error').html('Confirm  password should be same as password');
                return false;
            }  
           
            if($('#dob').val() !='')
            {
                if (agecount()) 
                {
                    $('#dob_error').html('');
                    
                }    else{$('#dob_error').html('Age should be atleast 18 years'); return false;}
            }
            
        
		    
            if(!($("#agree").attr('checked')))
            {
                //alert( "You should agree with Terms and Conditions");
                $("#check_confirm").html('Please select the checkbox to agree to the terms &amp; conditions');
                return false;
            }else
            {$("#check_confirm").html('');}
		
            var data=$('#User_username').val();
            var token=$('input[name="YII_CSRF_TOKEN"]').val();
            //            $.ajax({
            //                url:"<?php //echo Yii::app()->createUrl('user/uniqueRecipientmail');  ?>",
            //                data:{username:data,YII_CSRF_TOKEN:token},//data for throwing the expected url
            //                type:"POST",//you can also use POST method
            //                dataType:"html",// you can also specify for the result for json or xml
            //                success:function(response){
            //                    if(response==1)
            //                    {  $('#email_error').html(""); 
            //                        document.forms["register-form"].submit();
            //                        /* document.location='<?php // echo Yii::app()->request->baseUrl.  	      		                                        '/index.php/user/registration';          ?>'; */
            //                    }                      
            //                    else 
            //                    {  
            //                    
            //                        //$('#email_error').html("Usename already exist in database"); 
            //                        $('#User_username_em_').show();
            //                        $('#User_username_em_').html('This email is already registered with us. Please login or try another email id.');
            //                     
            //                    }              
            //                } 
            //            });
            document.forms["register-form"].submit();
            return false;
	   
        }
    } 
    
    
    function agecount() {
        var dob = $('#dob').val();
        dob = dob.split("/");
        var dd = dob[0];
        var mm = dob[1];;
        var yy = dob[2];;
        var age = 18;

        var mydate = new Date();
        mydate.setFullYear(yy, mm-1, dd);

        var currdate = new Date();
        currdate.setFullYear(currdate.getFullYear() - age);
        if ((currdate - mydate) < 0){
            return false;
        }
        else
        {
            return true;
        }   
    }
 
</script>
