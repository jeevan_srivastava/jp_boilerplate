<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/style.css" />
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/jquery-ui-1.8.21.custom.css" />
<?php
$activeCurrency = Currency::model()->findByAttributes(array('active' => '1', 'id' => Yii::app()->session['currency']));
$currency_loop = SliderCurrencyInterval::model()->findAllByAttributes(array('currency_id' => Yii::app()->session['currency'], 'active' => '1'));
if ($activeCurrency) {
    $initialAmount = $activeCurrency->initial_amount;
    $amountSymbol = $activeCurrency->currency_symbol;
    $currencyName = $activeCurrency->currency_name;
    $finalAmount = $activeCurrency->final_amount;
}
?>
<?php $this->widget('common.extensions.LangBox'); ?>
<p>&nbsp;</p><p>&nbsp;</p>
<?php $this->widget('common.extensions.Allcurrency'); ?>
<?php $form = $this->beginWidget('CActiveForm', array('id' => 'event-form', 'enableAjaxValidation' => false, 'htmlOptions' => array('enctype' => 'multipart/form-data'), 'action' => $this->createUrl('event/eventmessage'))); ?>
<div id="outer_body_container">
    <div class="main_container">
        <div class="step2 grid_14 beta prefix_1 suffix_1">
            <div class="outer_profile">
                <div class="profile1">
                    <div class="profile">
                        <div class="name_head"> <small> <strong><?php echo Yii::t('translation', 'contribute_to_giftpool'); ?></strong> </small></div>
                    </div>
                </div>
            </div>
                    
            <div class="content">
                <div class="mid_curve">
                    <div class="top_curve">
                        <div class="bottom_curve">
                            <div class="main_text">
                                <div class="big_roof"> <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/big_roof.gif" alt="" title=""/> </div>
                                <div class="tool_tip1"> <span><?php echo Yii::t('translation', 'your_gift_amount'); ?> </span> <small><em><?php echo "{$amountSymbol}"; ?></em><div id=spangiftamount2><?php echo $initialAmount ?></div></small> </div>
                                <div class="get_payment grid_12 prefix_1 alpha">
                                    <h3><?php echo Yii::t('translation', 'add_amount_to_gift'); ?> </h3>
                                    <small class="instruction"><?php echo Yii::t('translation', 'amounts_in'); ?>  <?php echo $currencyName; ?> </small>
                                    <div class="slider">
                                        <div id=tooltip1 class="tool_tip2" style='display:none'>
                                            <div class="tool_content">
                                                <div class="arrow1"> <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/arrow_bottom.png" alt="" title=""/> </div>
                                                <span>
                                                    <small class="rupee"><?php echo "{$amountSymbol}"; ?></small>
                                                    <div id=spangiftamount><?php echo $initialAmount ?></div>
                                                </span> 
                                            </div>
                                        </div>
                                        <div class="slider_rail" style='display:none'> 
                                            <span class="yello_bar"> <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/slider_yellow.png"/> </span> 
                                        </div>

                                        <div id=slider style='width:665px;z-index:20'></div>


                                        <ul class="price">
                                            <?php
                                            if (isset($currency_loop)) {
                                                for ($n = 0; $n < count($currency_loop); $n++) {
                                                    if ($n == 0) {
                                                        $class = 'first';
                                                    } else if ($n == count($currency_loop) - 1) {
                                                        $class = 'last';
                                                    } else {
                                                        $class = '';
                                                    }
                                                    ?>  
                                                    <li <?php if ($class != '') { ?>class="<?php echo $class; ?>"<?php } ?> style='padding-right:80px'><a href=# onclick="changeSlider(<?php echo $currency_loop[$n]->value; ?>)"><?php echo $currency_loop[$n]->value; ?></a></li> 
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </ul>
                                    </div>
                                    <div class="button"> 
                                        <a class="continue_btn" href="#" onclick="document.getElementById('event-form').submit();return false;"><?php echo Yii::t('translation', 'continue'); ?> </a><br />
                                        <a class="cancel" onclick="return callcancellight()" href="javascript:void(0);"><?php echo Yii::t('translation', 'cancel_event'); ?>  </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="modal">
    <div id="heading">

    </div>

    <div id="popupcontent">
    </div>
</div>
<input type=hidden id="amount" name=Event[amount] value='<?php echo $initialAmount ?>'>          
<input type="hidden" id="Event_next_step" name="Event[next_step]" value="message"/>     
<input type="hidden" id="currencyName" name="currencyName" value="<?php echo $currencyName; ?>"/>    
<?php $this->endWidget(); ?>    
<script>
    function selectAmount(val)
    {
        if(document.getElementById("amount").value>0)$("#amtli"+document.getElementById("amount").value).removeClass("active");
        $("#amtli"+val).addClass("active");

        document.getElementById("spangiftamount").innerHTML=val;
        document.getElementById("spangiftamount2").innerHTML=val;
        document.getElementById("amount").value=val;
        //alert(document.getElementById("amount").value);
        return false;
    }
    function calculateCharcount()
    {
        var maxchar=175;
        var message=document.getElementById("messagetxt").value
        if(message.length>maxchar)
        {
            document.getElementById("messagetxt").value=message.substring(0,175);
            message=document.getElementById("messagetxt").value;
        }
        document.getElementById("charcount").innerHTML=(maxchar-message.length);
    }


    var slider=$('#slider').slider({
        range: "min",
        value: <?php echo $initialAmount ?>,
        min: <?php echo $initialAmount ?>,
        max: <?php echo $finalAmount ?>,
        step: 10,
        slide: function( event, ui ) {
            if($('#currencyName').val()=='INR'){
                document.getElementById("tooltip1").style.left=(-71+(ui.value*665/2400))+"px";
            }
            if($('#currencyName').val()=='DOLLAR'){
                document.getElementById("tooltip1").style.left=(-141+(ui.value*665/70))+"px";
            }            
            document.getElementById("tooltip1").style.display="block";

            selectAmount(ui.value);
        },
        stop: function(event, ui) {document.getElementById("tooltip1").style.display="none";}
    });
    function changeSlider(val) {
        slider.slider( "value", val );
        selectAmount(val);
        return false;
    }
</script>
<style>
    .ui-state-default, .ui-widget-content .ui-state-default, .ui-widget-header .ui-state-default {
        background: url("<?php echo Yii::app()->request->baseUrl; ?>/images/scroll_btn.png") repeat-x scroll 50% 50%;
        border: 0px;
        z-index: 20;
    }    
    .ui-slider .ui-slider-handle{
        height: 5.2em;
        width: 5.2em;
        margin-top: -1.9em;
        margin-left: -1.9em;
        z-index: 22;
    }
</style>    
