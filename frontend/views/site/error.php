<?php
/**
 * error.php
 *
 * General view file to display error messages
 * @see errHandler at the main.php configuration file
 * @author: Hemendra <hemendra@t9l.com>
 * 
 */
$this->pageTitle .= ' - Error';
?>

<div class="error_page">
    <?php
    if (isset($message)):
        ?>
        <div><?php echo CHtml::encode($message) ?></div>
    <?php else: ?>
        <div>There was an error trying to process this request and we have made a note of it. Please try again after sometime.</div>
    <?php endif; ?>
</div>