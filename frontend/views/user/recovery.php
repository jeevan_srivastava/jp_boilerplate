<?php $this->pageTitle = Yii::app()->name . ' - ' . Yii::t('recovery', "Restore"); ?>
<h1><?php echo Yii::t('recovery', "Recover Password"); ?></h1>

<?php if (Yii::app()->user->hasFlash('recoveryFailMessage')): ?>
    <div class="alert in alert-block fade alert-error">
        <a data-dismiss="alert" class="close">×</a>
        <?php echo Yii::app()->user->getFlash('recoveryFailMessage'); ?>
    </div>
<?php endif; ?>

<?php if (Yii::app()->user->hasFlash('recoveryMessage')): ?>
    <div class="alert in alert-block fade alert-success">
        <a data-dismiss="alert" class="close">×</a>
        <?php echo Yii::app()->user->getFlash('recoveryMessage'); ?>
    </div>
<?php else: ?>

    <div class="form">
        <?php echo CHtml::beginForm(); ?>

        <?php echo CHtml::errorSummary($form,'','', array('class' => 'alert-error')); ?>

        <div class="row">
            <?php echo CHtml::activeLabel($form, 'login_or_email'); ?>
            <?php echo CHtml::activeTextField($form, 'login_or_email') ?>
            <p class="hint"><?php echo Yii::t('recovery', "Please enter your login or email addres."); ?></p>
        </div>

        <div class="row submit">
            <?php echo CHtml::submitButton(Yii::t('recovery', "Recover")); ?>
        </div>

        <?php echo CHtml::endForm(); ?>
    </div><!-- form -->
<?php endif; ?>