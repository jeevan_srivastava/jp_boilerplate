<?php $this->pageTitle=Yii::app()->name . ' - '.Yii::t('recovery',"Change Password");

?>
 <?php if (Yii::app()->user->hasFlash('change_password')): ?>

<div class="alert in alert-block fade alert-error">
        <a data-dismiss="alert" class="close">×</a>
	<?php echo Yii::app()->user->getFlash('change_password'); ?>
</div>

<?php endif; ?>
<h1><?php echo Yii::t('recovery',"Change Password"); ?></h1>


<div class="form">
<?php echo CHtml::beginForm(); ?>
<?php echo CHtml::hiddenField('passcode', $p ,array('id'=>'passcode')); ?>
	<p class="note"><?php echo Yii::t('recovery','Fields with <span class="required">*</span> are required.'); ?></p>
	<?php echo CHtml::errorSummary($form); ?>
	
	<div class="row">
	<?php echo CHtml::activeLabelEx($form,'password'); ?>
	<?php echo CHtml::activePasswordField($form,'password'); ?>
	<p class="hint">
	<?php echo Yii::t('recovery',"Minimal password length 8 symbols."); ?>
	</p>
	</div>
	
	<div class="row">
	<?php echo CHtml::activeLabelEx($form,'verifyPassword'); ?>
	<?php echo CHtml::activePasswordField($form,'verifyPassword'); ?>
	</div>
	
	
	<div class="row submit">
	<?php echo CHtml::submitButton(Yii::t('recovery',"Save")); ?>
	</div>

<?php echo CHtml::endForm(); ?>
</div><!-- form -->