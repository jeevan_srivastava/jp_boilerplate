<?php

/**
 * m120805_131754_user_table_migration.php
 *
 * @author: Hemendra <hemendra@t9l.com>
 * 
 */
class m120805_131754_user_table_migration extends CDbMigration {

    public function up() {
        $this->execute("CREATE TABLE IF NOT EXISTS `tbl_backend_user` (
                            `id` int(11) NOT NULL AUTO_INCREMENT,
                            `username` varchar(45) DEFAULT NULL,
                            `password` varchar(255) DEFAULT NULL,
                            `salt` varchar(255) DEFAULT NULL,
                            `password_strategy` varchar(50) DEFAULT NULL,
                            `requires_new_password` tinyint(1) DEFAULT NULL,
                            `email` varchar(255) DEFAULT NULL,
                            `login_attempts` int(11) DEFAULT NULL,
                            `login_time` int(11) DEFAULT NULL,
                            `login_ip` varchar(32) DEFAULT NULL,
                            `validation_key` varchar(255) DEFAULT NULL,
                            `create_time` int(11) DEFAULT NULL,
                            `update_time` int(11) DEFAULT NULL,
                            `status` tinyint(1) NOT NULL DEFAULT '0',
                            PRIMARY KEY (`id`),
                            UNIQUE KEY `username` (`username`),
                            UNIQUE KEY `email` (`email`)
                          ) ENGINE=InnoDB  DEFAULT CHARSET=utf8");



        $this->execute("CREATE TABLE IF NOT EXISTS `tbl_user` (
                    `id` int(11) NOT NULL AUTO_INCREMENT,
                    `fname` varchar(50) NOT NULL,
                    `lname` varchar(50) NOT NULL,
                    `dob` datetime NOT NULL,
                    `username` varchar(255) NOT NULL,
                    `password` varchar(255) NOT NULL,
                    `salt` varchar(255) DEFAULT NULL,
                    `password_strategy` varchar(50) DEFAULT NULL,
                    `requires_new_password` tinyint(1) DEFAULT NULL,
                    `status` tinyint(1) NOT NULL COMMENT '0->notverified,1->confirm,2->dummy',
                    `social_id` varchar(100) NOT NULL,
                    `social_id_type` varchar(30) NOT NULL,
                    `user_sex` varchar(8) NOT NULL,
                    `user_photo` varchar(60) NOT NULL,
                    `user_referrer_type` varchar(11) NOT NULL,
                    `user_creation_ip` varbinary(16) NOT NULL,
                    `user_lastlogin_ip` varbinary(16) NOT NULL,
                    `verification_code` varchar(50) DEFAULT NULL,
                    `has_agreed_terms` tinyint(1) NOT NULL DEFAULT '0',
                    `is_age_eighteen_above` tinyint(1) NOT NULL DEFAULT '0',
                    `logout_from_fb` tinyint(1) NOT NULL DEFAULT '0',
                    `ask_to_logout` tinyint(1) NOT NULL DEFAULT '0',
                    `is_locked_out` tinyint(1) NOT NULL DEFAULT '0',
                    `lock_out_time` datetime NOT NULL,
                    `login_attempts` tinyint(1) NOT NULL DEFAULT '0',
                    `validation_key` varchar(255) DEFAULT NULL,
                    `created` datetime DEFAULT NULL,
                    `modified` datetime DEFAULT NULL,
                    `last_login_time` datetime DEFAULT NULL,
                    `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
                    PRIMARY KEY (`id`),
                    UNIQUE KEY `user_username` (`username`)
                  ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1");


        /* add demo user to frontend */
        /* username and email are same */
        $demoUser = new User();
        $demoUser->username = "demo@t9l.com";
        $demoUser->password = "demot9l";

        $demoUser->insert();
        /* add admin user to backend*/
        /*username and email are diffrent . user can login by email or useranme*/
        $adminUser = new BackendUser();
        $adminUser->username = "admin";
        $adminUser->email = "admin@t9l.com";
        $adminUser->password = "admint9l";

        $adminUser->insert();
    }

    public function down() {
        $this->dropTable('tbl_backend_user');
        $this->dropTable('tbl_user');
    }

}